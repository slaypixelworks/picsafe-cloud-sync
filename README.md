# PicSafe Cloud Sync

PicSafe Cloud Sync is a command-line tool that lets PicSafe Enterprise admins run a script that downloads, decrypts and stores PicSafe Reports that have been previously uploaded to the PicSafe Cloud by users within a PicSafe Enterprise Account.


## Definitions

### PicSafe App

PicSafe is a super easy-to-use clinical photography app for taking patient photos, on a smartphone, while complying with privacy regulations. For more details please see <https://picsafe.com/>

### PicSafe Report

A PicSafe Report is an encrypted file that contains photo, video or audio files that have been securely captured in the PicSafe app. It also contains documentation of consent that is captured. For more details please see <https://picsafe.com/documentation/#contents-of-a-report/>

### PicSafe Cloud

PicSafe enables you to capture photos, video and audio and upload reports to the "PicSafe Cloud". The PicSafe Cloud is, essentially, a temporary storage server. It serves as a place where encrypted PicSafe reports can be temporarily stored while waiting for other systems to "get" them. There are PicSafe Clouds in Australia, Canada, Germany, Ireland, the UK, and the USA. For more details please see <https://picsafe.com/documentation/#picsafe-cloud/>

### PicSafe Enterprise Account

PicSafe can be deployed enterprise-wide and integrated into existing medical record systems. PicSafe user can set up a PicSafe Enterprise Account at <https://my.picsafe.com/> or they can be part of a PicSafe Enterprise Account set up by someone else. The person who set up the account is the "Administrator".


### PicSafe Key Server

When PicSafe reports are encrypted, the app fetches a key from a PicSafe Key Server. Then when a report is to be decrypted, a key is requested from the same Key Server and it's access is logged. This means the Key Server keeps an audit trail of who has accessed a PicSafe report. It is possible for enterprises to run their own Key Server or you can use the default one run by PicSafe. For more details please see <https://picsafe.com/au/documentation/#running-a-private-server>

### PicSafe User Server

This is a server operated by PicSafe that is used for authenticating PicSafe users. It does not have any patient data travelling through it. It simply stores user preferences.


## Pre-requisites

In order to use this tool you need to be an administrator to a PicSafe Enterprise Account. There are two pieces you need from your Enterprise account. Both can be found on Enterprise Account Information page (<https://my.picsafe.com/enterprise).>

* **API Key** is used to identify your Enterprise when making calls to my.picsafe.com from the command line script. This is how we know which reports you have permission to access.

* **Key Server Token** is the token that contains all information about your Enterprise and identifies it to the Key Server. Key Server holds the keys for decryption of any report in the system and keeps the log of everyone requesting them for each report. This is how we know who and when the requested the key for each particular report decryption was accessed.


## What This Script Can Be Used For

This script can act as "middleware". It downloads PicSafe reports from the PicSafe Cloud, decrypts them, and either stores then locally (in a specified directory). If required it can then be used or modified to transfer the files into other systems. In this sample repository we show an integration with MedTech32, but the script can be modified or built upon to do the same with other EMR/EHR systems.


## Overview - How This Works

1. A PicSafe user (that is part of a PicSafe Enterprise account) uploads a report to the PicSafe Cloud.
2. This script polls the PicSafe "User Server" asking if there are any new reports for it to download. If there are, it replies with the location in which the reports can be downloaded.
3. The script downloads the encrypted reports (.pifsafe file).
4. The script gets the location of the Key Server from the report.
5. The script requests a key from the Key Server.
6. The script decrypts the report.
7. The script places the decrypted file (zipped) into a specified directory.
8. (Optionally) the script connects with the MedTech database and places the file(s) into MedTech32.


## Usage

The PicSafe Sync script is not continuous. You will need to run it periodically to poll for updates.

    $ ./picsafe_sync \
        -k <api_key> \
        -o <key_server_token> \
        -e <encrypted_report_directory> \
        -d <decrypted_report_directory> \
        [-m <path_to_ehrconnect_json>]

* **api_key** -- your Enterprise API Key (see above)
* **key_server_token** -- your Enterprise Key Server Token (see above)
* **encrypted_report_directory** -- path to a directory for holding encrypted reports. This directory serves as the list of all reports you already have. Any reports that are not in this directory will be downloaded, decrypted and saved into **decrypted_report_directory**.
* **decrypted_report_directory** -- path to a directory for holding decrypted ZIP'ed reports.
* **ehrconnect.json** -- configuration file for MedTech32 ODBC database connection. If this parameter is specified, the script attempts to use the configuration file to connect to the database and update it by inserting the record in attachments table for each found patient.


## Details of operation

1. The PicSafe Sync script requests the list of all reports belonging to users being part of your Enterprise. Enterprise is identified by the provided **API Key**.
2. After the list is received, the script analyzes the **encrypted_report_directory** to identify new reports submitted since the last syncrhonization.
3. For each new report a decryption key is requested from the key server used to issue the encryption key. Information about the key server is encoded into the report. Your enterprise is identified by sending **key_server_token** to the Key Server.
4. Decrypted report is saved to the **decrypted_report_directory**.
5. If a `mt32 configuration file path` is specified (-m option), the script attempts to connect to the database and insert attachment rows for each decrypted image and associate them with a patients found in the database. This is done through the Powershell script (`bin/add_attachment.ps1`).


## Compiled Versions

In the `\builds` directory there are packaged versions for Linux (x86), Linux (x86_64), OSX and Windows.

* picsafe_sync-1.0.0-linux-x86.tar.gz
* picsafe_sync-1.0.0-linux-x86_64.tar.gz
* picsafe_sync-1.0.0-osx.tar.gz
* picsafe_sync-1.0.0-win32.tar.gz

They include a complete Ruby interpreter, used Ruby Gems and the script library folder. This means you should have have to install anything else to run the script.


## Packaging

This repository contains all tools necessary for packaging a self-containing archive for Linux, Mac OS X and Windows. You should note that only Ruby 2.2 is supported at the moment, as that's what the packaging tool uses internally.

Check that you have Ruby 2.2.x installed (and you are using it):

    $ ruby --version

If not, check to see if you have Rbuy Virtual Manager (RVM) installed:

    $ rvm --version

If you don't, first install it (https://rvm.io/rvm/install).

Check to see what versions of Ruby you have:

    $ rvm list

If you have version 2.2.x, change to using 2.2.x:

    $ rvm use ruby-2.2.4

If you don't have it, download and install Ruby 2.2.x (https://www.ruby-lang.org/en/downloads/).

Once installed, go back two steps and check to see what versions of Ruby you have running.

Install Bundler

    $ gem install bundler

To create packages, you need to run these commands:

    $ bundler install
    $ rake package:linux:x86
    $ rake package:linux:x86_64
    $ rake package:osx
    $ rake package:win32

Or replace individual package commands with one-for-all:

    $ bundler install
    $ rake package

Commands above will create `.tar.gz` (for Linux and Mac) and `.zip` (for Windows) archives with a single `picsafe_sync` command in the root of them. They will also include complete Ruby interpreter, used Ruby Gems and the script library folder.


## Using the MedTech32 Connector

As stated above, there is a sample MedTech32 Connector included in this repository. If a `mt32 configuration file path` is specified (-m option), the script attempts to connect to the database and insert attachment rows for each decrypted image and associate them with a patients found in the database. This is done through the Powershell script (`bin/add_attachment.ps1`).

An example of what a the `mt32 configuration file path` looks like, see `/test/ehrconnect.json`
