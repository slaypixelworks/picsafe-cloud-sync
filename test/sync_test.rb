require 'minitest/autorun'
require 'sync'
require 'settings'
require 'yaml'

class SyncTest < Minitest::Test

  ENCRYPTED_REPORT_PATH = File.join(File.dirname(__FILE__), "/fixtures/", "0000000021527617605398.picsafe")
  DECRYPTION_KEY        = "930bcb67-ebb1-40af-bd10-f13f9c628b5a"
  KEY_SERVER_TOKEN      = YAML.load(File.open(File.join(File.dirname(__FILE__), "secrets.yml")).read)["key_server_token"]

  def test_report_id_from_url
    src_url = "https://au.picsafe.amazonaws.com/upload/OUSl7N1fTM-0000000011529057076073.picsafe"

    id = sync.report_name_from_url(src_url)
    assert_equal "OUSl7N1fTM-0000000011529057076073.picsafe", id
  end

  def test_get_key_server
    assert_equal "https://key.picsafe.com/api", sync.get_key_server(ENCRYPTED_REPORT_PATH)
  end

  def test_to_query
    assert_equal "foo=bar&bar=b+z", sync.to_query(foo: "bar", bar: "b z")
  end

  def test_get_decryption_key
    assert_equal DECRYPTION_KEY, sync.get_decryption_key("https://key.picsafe.com/api", "0000000021527617605398")
  end

  def test_decrypt_report
    assert sync.decrypt_report(ENCRYPTED_REPORT_PATH, DECRYPTION_KEY, "0000000011529057076073")
  end

  def sync
    @sync ||= Sync.new(settings)
  end

  def settings
    s = Settings.new("", File.join(File.dirname(__FILE__), "..", "tmp"))
    s.key_server_token = KEY_SERVER_TOKEN
    s
  end

end
