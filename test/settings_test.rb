require 'minitest/autorun'
require 'settings'

class SettingsTest < Minitest::Test

  def test_config_init
    c = Settings.new("api_key", "/path")
    assert_equal "api_key", c.api_key
    assert_equal "/path", c.encrypted_path
  end

end
