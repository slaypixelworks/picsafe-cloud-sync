param (
    [string]$config_path="e:\ehrconnect.json",
    [string]$patient_id="222222",
    [string]$attachment_path="e:\someimage.jpg"
)

If (!(Test-Path -Path $config_path)) {
    Write-Output "config_path points to a missing file ($config_path)"
    Exit 1
}

If (!(Test-Path -Path $attachment_path)) {
    Write-Output "attachment_path points to a missing file ($attachment_path)"
    Exit 2
}

# Read config
$config = (Get-Content -Path $config_path) -join '' | ConvertFrom-Json

$dsn = $config.dsnHere

# Connect to ODBC
$conn = New-Object System.Data.Odbc.OdbcConnection
$conn.ConnectionString = "DSN=$dsn;"
$conn.open()

$query = $config.getIdSQL -replace '\[NHID\]', $patient_id
$cmd = New-object System.Data.Odbc.OdbcCommand($query, $conn)
$ds = New-Object system.Data.DataSet
(New-Object system.Data.odbc.odbcDataAdapter($cmd)).fill($ds) | out-null

$count = $ds.Tables[0].Rows.Count
if ($count -lt 1) {
    Write-Output "Patient with ID $patient_id not found"
    $conn.close()
    Exit 3
} else {
    $patient_id = $ds.Tables[0].Rows[0].PATIENTID

    $attachment_name = $attachment_path.Split('\') | Select-Object -Last 1

    $query = $config.insertAttachmentSQL
    $query = $query -replace '\[patientID\]', $patient_id
    $query = $query -replace "'\[currentDateTime\]'", $config.dbCurrentTimeSQL
    $query = $query -replace '\[photoname\]', $attachment_name
    $query = $query -replace '\[fullphotopath\]', $attachment_path
    $query = $query -replace '\[filelength\]', (Get-Item -Path $attachment_path).Length
    $query = $query -replace '\[staffname\]', $config.staffname
    $query = $query -replace '\[computer\]', $config.computer
    $query = $query -replace '\[location\]', $config.location

    $cmd = New-object System.Data.Odbc.OdbcCommand($query, $conn)
    $res = $cmd.ExecuteNonQuery()

    $conn.close()
    Exit 0
}
