require File.expand_path('mt32/config', File.dirname(__FILE__))

require 'rubygems'
require 'zip'

class Mt32

  class Error < StandardError; end

  def initialize(config_path)
    @config_path = config_path
    @config = Mt32::Config.new(config_path)
  end

  def process(decrypted_path)
    patient_id = get_patient_id(decrypted_path)

    if patient_id.to_s == ""
      report_name = decrypted_path.split('/').last
      puts "  #{report_name} Patient ID isn't specified. Skipping."
      return
    end

    store_attachments(patient_id, decrypted_path)
  end

  def get_patient_id(decrypted_path)
    Zip::File.open(decrypted_path) do |zip_file|
      zip_file.each do |entry|
        if entry.name == "metadata.json"
          data = JSON.parse(entry.get_input_stream.read)
          return data["report"]["patient"]["patientid"]
        end
      end
    end

    raise Error, "Report doesn't contain metadata.json"
  rescue NoMethodError
    raise Error, "Report doesn't contain valid metadata.json (looking for report.patient.patientid key)"
  end

  def store_attachments(patient_id, decrypted_path)
    Zip::File.open(decrypted_path) do |zip_file|
      zip_file.each do |entry|
        if entry.name =~ /\.jpg$/ && entry.name !~ /thumbnail/
          store_attachment(patient_id, entry)
        end
      end
    end
  end

  def store_attachment(patient_id, entry)
    # Make it so that we store attachments in <parentPhotoDir>/<patient_id>/<filename.jpg>
    attachments_path = File.join(@config.parent_photo_dir, patient_id)
    attachment_path = File.join(attachments_path, entry.name)

    # Create patient attachment directory
    unless File.exists?(attachments_path)
      FileUtils.mkdir_p(attachments_path)
    end

    # If attachment already exists skip further processing
    return if File.exists?(attachment_path)

    # Write attachment data
    File.open(attachment_path, "wb") do |file|
      file.write(entry.get_input_stream.read)
    end

    cmd = File.expand_path('..\bin\add_attachment.ps1', File.dirname(__FILE__))
    success = system('powershell', cmd, "-config_path", @config_path, "-patient_id", patient_id, "-attachment_path", attachment_path)
    if success
      puts "  Added to db: #{attachment_path}"
    else
      case $?.to_s
      when '1'
        raise Error, "Configuration file (ehrconnect.json) is missing"

      when '2'
        raise Error, "Attachment file (#{attachment_path}) is missing"

      when '3'
        raise Error, "Patient with this NHID (#{patient_id}) is not in database"

      else
        raise Error, "Unknown error: '#{$?}'"
      end
    end
  end

end
