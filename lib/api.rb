require 'open-uri'
require 'json'

class Api

  class Error < StandardError; end

  def self.list_report_urls(settings)
    json = open(settings.base_url + "/api/enterprise/reports?api_key=#{settings.api_key}").read
    data = JSON.parse(json)

    if data["status"] == "OK"
      return data["reports"]
    else
      raise Error, data["message"]
    end
  end

end
