class Mt32
  class Config

    class Error < StandardError; end

    def initialize(path)
      @data = JSON.parse(File.read(path))
      validate
    end

    def validate
      # Put validation of required properties here and raise Error
      # if something important is missing
      raise Error, "parentPhotoDir is missing from config" if @data["parentPhotoDir"].to_s == ""
    end

    def parent_photo_dir
      @data["parentPhotoDir"]
    end
  end
end
