require File.expand_path('api', File.dirname(__FILE__))
require File.expand_path('mt32', File.dirname(__FILE__))
require 'fileutils'
require 'cgi'

require 'rubygems'
require 'zip'
require 'ruby_rncryptor'

class Sync

  class DecryptionError < StandardError; end

  def initialize(settings)
    @settings = settings
  end

  def perform
    report_urls = Api.list_report_urls(@settings)
    download_missing(report_urls)
  end

  def download_missing(report_urls)
    mt32 = nil
    if @settings.mt32?
      mt32 = ::Mt32.new(@settings.mt32_config_path)
    end

    report_urls.each do |url|
      name = report_name_from_url(url).gsub(".picsafe", ".zip")
      if report_missing?(name)
        begin
          encrypted_path = download_report(url)
          decrypted_path = decrypt_report(encrypted_path)

          mt32.process(decrypted_path) if mt32
        rescue OpenURI::HTTPError => e
          if e.message =~ /403/
            puts "  Gone. Skipping."
          else
            raise e
          end
        rescue DecryptionError => e
          puts "  #{e}. Skipping."
        end
      end
    end
  end

  def report_missing?(name)
    path = File.expand_path(name, @settings.decrypted_path)
    !File.exist?(path)
  end

  # Downloads encrypted report from the given URL and stores in the #settinsg.encrypted_path.
  # Returns the path to the encrypted .picsafe file.
  def download_report(url)
    unless File.exist?(@settings.encrypted_path)
      FileUtils.mkdir_p(@settings.encrypted_path)
    end

    report_filename = report_name_from_url(url)
    dst_path = File.expand_path(report_filename, @settings.encrypted_path)
    puts "- #{url}"

    download = open(url)
    IO.copy_stream(download, dst_path)

    return dst_path
  end

  # Decrypts the report
  def decrypt_report(encrypted_report_path)
    key_server_url = get_key_server(encrypted_report_path)
    report_id = report_id_from_url(encrypted_report_path)
    report_name = report_name_from_url(encrypted_report_path)
    decryption_key = get_decryption_key(key_server_url, report_id)
    return do_decrypt_report(encrypted_report_path, decryption_key, report_name)
  end

  def report_name_from_url(url)
    return url.split('/').last
  end

  def report_id_from_url(url)
    url.split('/').last.split('-').last.gsub('.picsafe', '')
  end

  def get_key_server(encrypted_report_path)
    Zip::File.open(encrypted_report_path) do |zip_file|
      zip_file.each do |entry|
        if entry.name == "package.json"
          return read_package_json(entry)
        end
      end
    end

    raise DecryptionError, "No package.json in #{encrypted_report_path}"
  end

  def read_package_json(entry)
    json = entry.get_input_stream.read
    data = JSON.parse(json)
    return data["keyserver_url"]
  end

  def get_decryption_key(key_server_url, report_id)
    query = to_query(report_id: report_id, token: @settings.key_server_token)
    uri = URI.parse("#{key_server_url}/decryption_key?#{query}")
    json = uri.read

    data = JSON.parse(json)
    if data["success"]
      return data["decryption_key"]
    else
      raise DecryptionError, "Unable to get decryption key (#{report_id}): #{data["error"]}"
    end
  end

  def do_decrypt_report(encrypted_report_path, decryption_key, report_name)
    unless File.exist?(@settings.decrypted_path)
      FileUtils.mkdir_p(@settings.decrypted_path)
    end

    Zip::File.open(encrypted_report_path) do |zip_file|
      zip_file.each do |entry|
        if entry.name == "package.encrypted"
          encrypted_data = entry.get_input_stream.read
          data = RubyRNCryptor.decrypt(encrypted_data, decryption_key)
          decrypted_file_path = File.expand_path(report_name.gsub(".picsafe", ".zip"), @settings.decrypted_path)
          File.open(decrypted_file_path, "wb") do |decrypted_file|
            decrypted_file.write(data)
          end

          return decrypted_file_path
        end
      end
    end

    raise DecryptionError, "No package.encrypted in #{encrypted_report_path}"
  end

  def to_query(hash)
    hash.map do |key, value|
      "#{CGI.escape(key.to_s)}=#{CGI.escape(value.to_s)}"
    end.compact * "&"
  end
end
