class Settings

  class ValidationError < StandardError; end

  attr_accessor :base_url
  attr_accessor :decrypted_path
  attr_accessor :encrypted_path
  attr_accessor :api_key
  attr_accessor :key_server_token
  attr_accessor :mt32_config_path

  def initialize(api_key, path)
    @api_key = api_key
    @decrypted_path = path
    @encrypted_path = path
    @key_server_token = nil
    @base_url = "https://my.picsafe.com"
  end

  def mt32?
    self.mt32_config_path.to_s.strip != ""
  end

  def validate
    raise ValidationError.new("API key is required") if @api_key.to_s.strip == ""
    raise ValidationError.new("Key server token is required") if @key_server_token.to_s.strip == ""
  end

end
